// Contains task model/schema of our application
const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({

	name: String,
	status: {
		type: String,
		default: "pending"
	}

});


// "module.exports" is a way for JS to treat this value as a package that can be used by other files
module.exports = mongoose.model("Task", taskSchema);