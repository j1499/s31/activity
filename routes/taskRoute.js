// Contains all of the endpoints of our application

// We need to use express' Router() function to achieve this

const express = require("express");

// Allows access to HTTP method middlewares that makes it easier to create routes for our application
const router = express.Router();

// The "taskController" allows us to use the functions defined in the taskController.js file 
const taskController = require("../controllers/taskController")

// Route to GET all the tasks
router.get('/', (req,res) => {
	taskController.getAllTasks().then(resultFromController => 
		res.send(resultFromController));
});

// Activity ----------------------------------------

router.get('/:id', (req,res) => {
	console.log(req.params)
	taskController.getTasks(req.params.id).then(resultFromController => res.send(resultFromController));
});

// Activity ----------------------------------------


// Route to CREATE a task
// This route expects to receive POST request at the URL "/tasks/"
router.post('/', (req,res) => {
	console.log(req.body);
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});


// Route to DELETING a task
router.delete('/:id', (req,res) => {
	console.log(req.params)
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController))
	// res.send(`Task ${resultFromController.name} is deleted!`)
});

// Route to UPDATE a task
router.put('/:id', (req,res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
});

// Activity ----------------------------------------

router.put('/:id/complete', (req,res) => {
	taskController.updateStatus(req.params.id, req.body.status).then(resultFromController => res.send(resultFromController));
})

// Activity ----------------------------------------

// Use "module.exports" to export the router object to use in the index.js file
module.exports = router;